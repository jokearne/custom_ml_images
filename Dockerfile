# Select a base image from which to extend
FROM registry.cern.ch/ml/kf-14-tensorflow-jupyter:v4

USER root

RUN apt-get update && apt-get install -y cmake

USER jovyan

# The following line is mandatory:
CMD ["sh", "-c", \
     "jupyter lab --notebook-dir=/home/jovyan --ip=0.0.0.0 --no-browser \
      --allow-root --port=8888 --LabApp.token='' --LabApp.password='' \
      --LabApp.allow_origin='*' --LabApp.base_url=${NB_PREFIX}"]
